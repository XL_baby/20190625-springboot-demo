package com.hx.demo.controller;

import com.hx.demo.domain.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @RequestMapping("user")
    public User UserInfo(){
        User user=new User();
        user.setId(1);
        user.setUserName("alice");
        user.setPassword("123");
        user.setEmail("aa@qq.com");
        return user;
    }
}
